/*BEGIN_LEGAL 
Intel Open Source License 

Copyright (c) 2002-2014 Intel Corporation. All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.  Neither the name of
the Intel Corporation nor the names of its contributors may be used to
endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
END_LEGAL */
/*
 *  This file contains an ISA-portable PIN tool for tracing memory accesses.
 */

#include <cstdio>
#include <cstdlib>
#include <unistd.h>

#include <fcntl.h>
#include <sys/user.h>
#include <zlib.h>

#include "pin.H"
#include "instlib.H"

//#define BINH_KERNEL_PATCH

#define PAGE_SHIFT 12 // user defined, workaround
#define PM_PSHIFT_OFFSET 55     // pfn is bits 0-54 in pagemap 
#define PM_PFRAME_MASK ((1LL << PM_PSHIFT_OFFSET) - 1)
#define PM_PFRAME(x) ((x) & PM_PFRAME_MASK)

typedef unsigned long long u64;
typedef unsigned long      u32;

enum { FASTFW_UNIT = 1000000000 };

static UINT64 FASTFW = 0;	// fastforward instructions
static UINT64 COLLECT = 0;	// collect instructions

// The running count of instruction is kept here
static int pagemap_fd;
static int kpageflags_fd;

static gzFile trace;

static LEVEL_BASE::PIN_MUTEX trace_guard;

static inline u64 rdtsc(void)
{
        if (sizeof(long) == sizeof(u64)) {
                u32 lo, hi;
                asm volatile("rdtsc" : "=a" (lo), "=d" (hi));
                return ((u64)(hi) << 32) | lo;
        }
        else {
                u64 tsc;
                asm volatile("rdtsc" : "=A" (tsc));
                return tsc;
        }
}


static bool collecting(bool instruction)
{
    static unsigned long count = 0;
    const unsigned long new_c =
        instruction ? __sync_add_and_fetch(&count, 1) : count;

    if (instruction && ((new_c % FASTFW_UNIT) == 0))
        fprintf(stderr, "Executed %luB instructions\n", new_c / FASTFW_UNIT );

    if (instruction && (new_c >= (FASTFW + COLLECT))) {
        fprintf(stderr, "END TRACE COLLECTION\n");
        PIN_Detach();
    }
    return new_c > FASTFW && new_c <= (FASTFW + COLLECT);
}


struct addr_info {
	enum {PTES_PER_CL = 8};
	VOID *addr;
	VOID *physaddr;
	uint64_t pfn[PTES_PER_CL];
	uint64_t pgd;
	uint64_t pud;
	uint64_t pmd;
	uint64_t pte;
	bool present[PTES_PER_CL];
	bool swapped[PTES_PER_CL];
	bool shared[PTES_PER_CL];
	bool thp[PTES_PER_CL];
	bool ksm[PTES_PER_CL];
	bool huge[PTES_PER_CL];
	bool writeable[PTES_PER_CL];

	addr_info(VOID *addr);
	void print(gzFile trace, VOID* ip, char type);

private:
	void populate(uintptr_t a);
};

addr_info::addr_info(VOID *addr_): addr(addr_)
{
    uintptr_t a = (uintptr_t)addr;
    const uintptr_t offset = a & (PAGE_SIZE - 1);
    a >>= PAGE_SHIFT;
    const unsigned idx = a & (PTES_PER_CL - 1);
    for (unsigned i = 0; i < PTES_PER_CL; ++i)
	    populate(a - idx + i);
    physaddr = (void*)((pfn[idx] << PAGE_SHIFT) | offset);
}
void addr_info::populate(uintptr_t a)
{
    const unsigned idx = a & (PTES_PER_CL - 1);

#ifdef BINH_KERNEL_PATCH
    uint64_t val[6];
#else
    uint64_t val[1];
#endif

    if (pread(pagemap_fd, val, sizeof(val), a * sizeof(val)) == -1) {
	perror("PREAD failed");
	exit(EXIT_FAILURE);
    }

    // read thp flag
    pfn[idx] = PM_PFRAME(val[0]);
    uint64_t flags = 0;
    if (pread(kpageflags_fd, &flags,
	sizeof(flags), pfn[idx] * sizeof(flags)) == -1) {
	perror("PREAD kpageflags failed");
	exit(EXIT_FAILURE);
    }
    huge[idx] = ((flags & (1ULL << 17)) != 0);
    thp[idx] = ((flags & (1ULL << 22)) != 0);
    ksm[idx] = ((flags & (1ULL << 21)) != 0);

#ifdef BINH_KERNEL_PATCH
    pgd = val[1];
    pud = val[2];
    pmd = val[3];
    pte = val[4];
    // page permissions
    writeable[idx] = (val[5] & (1ULL << 1)) != 0;
#else
    pgd = pud = pmd = pte = 0;
    writeable[idx] = false;
#endif

    present[idx]  = (val[0] & (1ULL << 63)) != 0;
    swapped[idx]  = (val[0] & (1ULL << 62)) != 0;
    shared[idx]   =  (val[0] & (1ULL << 61)) != 0;
}

void addr_info::print(gzFile trace, VOID* ip, char type)
{
    if (!trace)
	return;

    LEVEL_BASE::PIN_MutexLock(&trace_guard);
    gzprintf(trace, "%lu:%p:%c:%p:%p:0x%lx:0x%lx:0x%lx:0x%lx:%llx",
             PIN_ThreadUid(),
	     ip, type, addr, physaddr, pgd, pud, pmd, pte, rdtsc());
    for (unsigned i = 0; i < PTES_PER_CL; ++i)
        gzprintf(trace,":%lx:%s:%s:%s:%s:%s:%s:%s", pfn[i],
                 present[i] ? "P": "NP", swapped[i] ? "SW" : "",
                 shared[i] ? "SH" : "", thp[i] ? "THP":"", huge[i] ? "HUGE":"",
                 ksm[i] ? "KSM" : "", writeable[i] ? "PW":"PR");
    gzprintf(trace, "\n");
    LEVEL_BASE::PIN_MutexUnlock(&trace_guard);
}

static VOID PrintAddr(gzFile trace, VOID *ip, VOID *addr, char type)
{
    if (collecting(type == 'I')) {
        addr_info info(addr);
        info.print(trace, ip, type);
    }
}

// Print a memory read record
VOID RecordMemRead(VOID * ip, VOID * addr)
{
    PrintAddr(trace, ip, addr, 'R');
}

// Print a memory write record
VOID RecordMemWrite(VOID * ip, VOID * addr)
{
    PrintAddr(trace, ip, addr, 'W');
}

// Print an inst execute record
VOID RecordInst(VOID * ip)
{
    PrintAddr(trace, ip, ip, 'I');
}

// Is called for every instruction and instruments reads and writes
VOID Instruction(INS ins, VOID *v)
{
    // Insert a call to docount before every instruction, no arguments
    // are passed
    // INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)docount, 
    //                IARG_INST_PTR, IARG_END);
    // Instruments memory accesses using a predicated call, i.e.
    // the instrumentation is called iff the instruction will actually be executed.
    //
    // On the IA-32 and Intel(R) 64 architectures conditional moves and REP
    // prefixed instructions appear as predicated instructions in Pin.
    INS_InsertPredicatedCall(
        ins, IPOINT_BEFORE, (AFUNPTR)RecordInst,
        IARG_INST_PTR, IARG_END);

    UINT32 memOperands = INS_MemoryOperandCount(ins);

    // Iterate over each memory operand of the instruction.
    for (UINT32 memOp = 0; memOp < memOperands; memOp++)
    {
        if (INS_MemoryOperandIsRead(ins, memOp))
        {
            INS_InsertPredicatedCall(
                ins, IPOINT_BEFORE, (AFUNPTR)RecordMemRead,
                IARG_INST_PTR,
                IARG_MEMORYOP_EA, memOp,
                IARG_END);
        }
        // Note that in some architectures a single memory operand can be
        // both read and written (for instance incl (%eax) on IA-32)
        // In that case we instrument it once for read and once for write.
        if (INS_MemoryOperandIsWritten(ins, memOp))
        {
            INS_InsertPredicatedCall(
                ins, IPOINT_BEFORE, (AFUNPTR)RecordMemWrite,
                IARG_INST_PTR,
                IARG_MEMORYOP_EA, memOp,
                IARG_END);
        }
    }
}

static VOID EndCollection()
{
      LEVEL_BASE::PIN_MutexLock(&trace_guard);
      if (trace) {
            gzputs(trace, "#eof\n");
            gzclose(trace);
            trace = NULL;
	    printf("Closed trace file\n");
      }
      LEVEL_BASE::PIN_MutexUnlock(&trace_guard);
}

VOID Detach(VOID *v)
{
    printf("Called Detach\n");
    EndCollection();
    exit(0);
}

/* This is called when the program finishes before collecting the requested
 * number of instructions */
VOID Fini(INT32 code, VOID *v)
{
    printf("Called Fini\n");
    EndCollection();
    exit(0);
}
/* ===================================================================== */
/* Commandline Switches                                                  */
/* ===================================================================== */
 KNOB<UINT64> KnobFastForward(KNOB_MODE_WRITEONCE, "pintool",
	"fw", "1", "fastforward instructions in billions");
 KNOB<UINT64> KnobCollect(KNOB_MODE_WRITEONCE, "pintool",
	"collect", "1", "fastforward instructions in billions");
 KNOB<string> KnobFile(KNOB_MODE_WRITEONCE, "pintool",
	"tracef", "pinatrace.gz", "trace filename");


/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */
INT32 Usage()
{
    PIN_ERROR( "This Pintool prints a trace of memory addresses\n"
              + KNOB_BASE::StringKnobSummary() + "\n");
    return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char *argv[])
{
    if (PIN_Init(argc, argv)) return Usage();
    FASTFW = KnobFastForward.Value() * FASTFW_UNIT;
    COLLECT = KnobCollect.Value() * 100000000;
    fprintf(stderr, "FASTFW instructions count: %ld\n", FASTFW);
    fprintf(stderr, "Collect instructions count: %ld\n", COLLECT);

    trace = gzopen(KnobFile.Value().c_str(), "wb");
    if (trace == NULL) {
	perror("Error opening tracefile");
	exit(EXIT_FAILURE);
    }

    LEVEL_BASE::PIN_MutexInit(&trace_guard);

    PIN_AddFiniFunction(Fini, 0);
    PIN_AddDetachFunction(Detach, NULL);
    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(Instruction, 0);

    pagemap_fd = open("/proc/self/pagemap", O_RDONLY);
    if (pagemap_fd < 0) {
	perror("Error opening /proc/self/pagemap");
	exit(EXIT_FAILURE);
    }

    kpageflags_fd = open("/proc/kpageflags", O_RDONLY);
    if (kpageflags_fd < 0) {
	perror("Error opening /proc/kpageflags");
	exit(EXIT_FAILURE);
    }

    // Never returns
    PIN_StartProgram();
    return 0;
}
